terraform {
  required_version = ">=1.3.1"

  backend "s3" {
    bucket         = "hqr.common.database.module.kojitechs.tf"
    dynamodb_table = "terraform-lock"
    key            = "path/env"
    region         = "us-east-1"
    encrypt        = "true"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
  assume_role {
    role_arn = "arn:aws:iam::${lookup(var.aws_account_id, terraform.workspace)}:role/Role_For-S3_Creation"
  }
  default_tags {
    tags = module.required_tags.aws_default_tags
  }
}

locals {
  operational_state = data.terraform_remote_state.operational_environment.outputs
  vpc_id            = local.operational_state.vpc_id
  public_subnet     = local.operational_state.public_subnets
  private_subnets   = local.operational_state.private_subnets
}


data "terraform_remote_state" "operational_environment" {
  backend = "s3"

  config = {
    region = "us-east-1"
    bucket = "operational.vpc.tf.kojitechs"
    key    = format("env:/%s/path/env", terraform.workspace)
  }
}

resource "aws_ecs_cluster" "ecs-jenkins" {
  name = "WEB"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

data "aws_region" "current" {}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "3.0.0"

  domain_name               = trimsuffix(data.aws_route53_zone.zone.name, ".")
  zone_id                   = data.aws_route53_zone.zone.zone_id
  subject_alternative_names = var.subject_alternative_names
}

resource "aws_route53_record" "dns_record" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "www.${var.dns_zone_name}"
  type    = "A"

  alias {
    name                   = module.jenkins_microservice.alb_dns_name
    zone_id                = module.jenkins_microservice.alb_zone_id
    evaluate_target_health = true
  }
}

resource "aws_efs_file_system" "jenkins_data" {
  creation_token = "${var.component_name}-jenkins-efs"

  performance_mode = "generalPurpose"
  tags = {
    Name = "${var.component_name}-jenkins-efs"
  }
}

resource "aws_security_group" "jenkins_data_allow_nfs_access" {
  name        = "${var.component_name}-jenkins-efs-allow-nfs"
  description = "Allow NFS inbound traffic to EFS"
  vpc_id      = local.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.component_name}-jenkins-efs-allow-nfs"
  }
}

resource "aws_security_group_rule" "jenkins_data_allow_nfs_access_rule" {
  security_group_id = aws_security_group.jenkins_data_allow_nfs_access.id
  description       = "Allow ecs service access to efs file system"
  type              = "ingress"
  from_port         = 2049
  to_port           = 2049

  protocol                 = "tcp"
  source_security_group_id = module.jenkins_microservice.ecs_sg_id
}

resource "aws_efs_mount_target" "jenkins_data_mount_targets" {
  count           = length(local.private_subnets)
  file_system_id  = aws_efs_file_system.jenkins_data.id
  subnet_id       = element(local.private_subnets, count.index)
  security_groups = [aws_security_group.jenkins_data_allow_nfs_access.id]
}

resource "aws_cloudwatch_log_group" "awslogs-group" {

  name = lower("/ecs/service/${var.cluster_name}")
  tags = {
    Name = lower("${var.cluster_name}")
  }
}

module "required_tags" {
  source = "git::https://github.com/Bkoji1150/kojitechs-tf-aws-required-tags.git?ref=v1.0.0"

  line_of_business        = var.line_of_business
  ado                     = var.ado
  tier                    = var.tier
  operational_environment = upper(terraform.workspace)
  tech_poc_primary        = var.tech_poc_primary
  tech_poc_secondary      = var.builder
  application             = var.application
  builder                 = var.builder
  application_owner       = var.application_owner
  vpc                     = var.vpc
  cell_name               = var.cell_name
  component_name          = var.component_name
}

module "jenkins_microservice" {
  source = "git::git@gitlab.com:kojibello/ecs-task-definition-tf.git?ref=v1.0.0"

  component_name                 = var.component_name
  cluster_name                   = aws_ecs_cluster.ecs-jenkins.arn
  container_name                 = var.container_name
  container_version              = var.container_version
  container_port                 = var.container_port
  target_group_health_check_path = "/login"

  certificate_arn        = module.acm.acm_certificate_arn
  ecs_service_subnet_ids = local.private_subnets
  vpc_id                 = local.vpc_id
  public_subnets         = local.public_subnet
  ecs_service_name       = lower(format("%s-%s", var.cell_name, var.component_name))

  ecs_container_port = {
    jenkins_master = {
      from_port = var.container_port
      to_port   = var.container_port
    }
    jenkins_agent = {
      from_port = var.worker_port
      to_port   = var.worker_port
    }
  }
  container_port_mappings = [
    {
      containerPort = var.container_port
      hostPort      = var.container_port
      protocol      = "tcp"
    },
    {
      containerPort = var.worker_port
      hostPort      = var.worker_port
      protocol      = "tcp"
    }
  ]
  container_log_configuration = {
    logDriver = "awslogs"
    options = {
      "awslogs-create-group"  = "true"
      "awslogs-group"         = aws_cloudwatch_log_group.awslogs-group.name
      "awslogs-region"        = var.aws_region
      "awslogs-stream-prefix" = var.container_name
    }
    secretOptions = null
  }
  volumes = [{
    name                        = "${var.component_name}-jenkins-efs"
    host_path                   = null
    docker_volume_configuration = []
    efs_volume_configuration = [{
      file_system_id          = aws_efs_file_system.jenkins_data.id
      root_directory          = "/"
      transit_encryption      = "ENABLED"
      transit_encryption_port = null
      authorization_config = [{
        access_point_id = aws_efs_access_point.fargate.id
        iam             = "ENABLED"
      }]
    }]
  }]
  container_mount_points = [
    {
      sourceVolume  = "${var.component_name}-jenkins-efs"
      containerPath = var.target_group_health_check_path
      readOnly      = false
    }
  ]
}
